FROM ubuntu:xenial

ENV ANSIBLE_CONFIG=./ansible.cfg

RUN apt-get update && \
    apt-get install -y software-properties-common make git apt-transport-https ca-certificates && \
    apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 9DC858229FC7DD38854AE2D88D81803C0EBFCD88 && \
    echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable" > /etc/apt/sources.list.d/docker.list && \
    apt-add-repository ppa:ansible/ansible && \
    apt-get update && \
    apt-get install -y ansible docker-ce python-pip jq && \
    pip install docker==3.3.0 awscli dnspython boto3 boto openshift pyyaml && \
    apt-get clean all && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
